// Using DOM
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


// Event listener - an interaction between the user and the web page.
 txtFirstName.addEventListener('keyup',(event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup',(event) => {
	spanFullName.innerHTML= txtFirstName.value + " " + txtLastName.value;
})

// --------------------------------------ACTIVITY SOLUTION-----------------------
/*
const updateFullName = () => {
	let txtFirstName = txtFirstName.value;
	let txtLastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);

*/